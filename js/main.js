const GameApi =
  {
    gameStatus:
      {
        gameId: null,
        name: null,
        opponentName: null,
        move: null,
        opponentMove: null,
        game: null,
      },


    getToken: () => fetch("https://java19.sensera.se/auth/token")
      .then((response) => response.text())
      .then((text) => sessionStorage.setItem('token', text)),

    storedToken: () => {
      return sessionStorage.getItem('token');
    },

    setName: () => {
      return fetch('https://java19.sensera.se/user/name', {
        method: 'POST',
        body: JSON.stringify({"name": "NiclasK"}),
        headers: {'token': sessionStorage.getItem('token'),'Content-Type': 'application/json;charset=UTF-8'},
      })

        .then((response) => response.text())
    },


    startGame: () => {
     return fetch('https://java19.sensera.se/games/start', {headers: {'token': sessionStorage.getItem('token')}})
        .then((response) => response.json())
    },

    gameList: () => {
      return fetch('https://java19.sensera.se/games', {headers: {'token': sessionStorage.getItem('token')}})
        .then((response) => response.json())
    },

    joinGame: (gameId) => fetch('https://java19.sensera.se/games/join/' + gameId, {headers: {'token': sessionStorage.getItem('token')}})
      .then((response) => response.json())
      .then((json) => this.gameStatus = json),

    gameStats: () => fetch('https://java19.sensera.se/games/status', {headers: {'token': sessionStorage.getItem('token')}})
      .then((response) => response.json())
      .then((json) => this.gameStatus = json),

    makeMove: (sign) => fetch('https://java19.sensera.se/games/move/' + sign, {headers: {'token': sessionStorage.getItem('token')}})
      .then((response) => response.json())
      .then((json) => this.gameStatus = json),


    gameId: (gameId) => fetch('https://java19.sensera.se/games/' + gameId, {headers: {'token': sessionStorage.getItem('token')}})
      .then((response) => response.json())
      .then((json) => this.gameStatus = json),
  };


if (sessionStorage.getItem('token') == null) {
  GameApi.getToken();
}

