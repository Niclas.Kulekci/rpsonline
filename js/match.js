
const result_p = document.querySelector(".result > p");

function refreshGames()
{
  GameApi.gameList()
    .then(games => {
      if (games.length === 0)
      {
        document.getElementById('games').innerHTML = "No Games Available"
        return;

      }
      let gameListHtml = games.map(game =>
      {
        return createGameList(game.gameId, game.name)

      })
        .join();
      document.getElementById('games').innerHTML = gameListHtml
    });
}

function createGameList(gameId, name)
{
  if (name === null || name === 'null')
    name = 'Anonymous';
  return '<div class="bg-secondary rounded shadow p"\n' +
    '<li class="text white d-flex flex-row justify-content-around align-items-center pb-1">\n' +
    '  <div class="text-white"> ' + name + ' </div>\n' +
    ' <button class="btn btn-primary text-white" onclick="joinTheGame(\'' + gameId + '\')"> Join </button>\n' +
    '</li>' +
    '<hr/>'
}

function startTheGame()
{
  GameApi.setName()
    .then(ignore =>
    {
      GameApi.startGame()
        .then(gameStatus =>
        {
          window.location.href = 'src/indexStartGame.html';
        });
    })
}

function joinTheGame(gameId)
{
  GameApi.setName()
    .then(ignore =>
    {
      GameApi.joinGame(gameId)
        .then(gameStatus =>
        {
          localStorage.setItem('joinedGame', 'true')
          window.location.href = 'src/indexJoinGame.html';
        });
    })
}

function refreshingStatusPS()
{
  setInterval(refreshGameStatus, 2000);
}

function refreshGameStatus ()
{
  GameApi.gameStats()
    .then(gameStatus =>{
      document.getElementById("user-label").innerHTML = gameStatus.name;
      document.getElementById("computer-label").innerHTML = gameStatus.opponentName;
      switch (gameStatus.game){
        case 'WIN':  win(gameStatus.move,gameStatus.opponentMove); break;
        case 'LOSE': lose(gameStatus.move,gameStatus.opponentMove); break;
        case 'DRAW': draw(gameStatus.move,gameStatus.opponentMove); break;
      }
    });
}

